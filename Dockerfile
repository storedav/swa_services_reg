FROM maven:latest as builder
ENV APP_HOME=/root/dev/app/

RUN mkdir -p $APP_HOME/src
WORKDIR $APP_HOME
COPY ./src $APP_HOME/src
COPY pom.xml $APP_HOME/pom.xml
RUN mvn package
RUN ls -la ./target
RUN cp ./target/SWA_services_reg*.jar ./target/SWA_services_reg.jar

FROM openjdk:17-jdk-alpine
LABEL Maintainer: storedav@fel.cvut.cz
ENV APP_HOME=/root/dev/app/
ENV DEPS_HOME=/root/dev/deps
WORKDIR /usr/app/
COPY --from=builder $APP_HOME/target/SWA_services_reg.jar ./SWA_services_reg.jar
ENTRYPOINT ["java", "-jar", "/usr/app/SWA_services_reg.jar"]